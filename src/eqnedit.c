#include "config-full.h"

#include "eqnedit-glade.h"
#include <mvc-classes.h>
#include "eqnedit-math-presentationtoken.h"
#include "eqnedit-math-fraction.h"
#include "eqnedit-math-container.h"
#include "eqnedit-canvasview-container.h"
#include "eqnedit-canvasview-token.h"
#include "eqnedit-canvasview-token-private.h"
#include "eqnedit-canvasview-fraction.h"
#include "eqnedit-canvasview-fraction-private.h"
#include "eqnedit-canvasview-rendersimple.h"
#include "eqnedit-canvasview-rendernull.h"

#include "eqnedit-document-object.h"


static void
print_indent (int indent)
{
	int i;
	for (i = indent; i > 0; i--) {
		printf ("  ");
	}
} /* print_indent */


#if 0
/* XXX create a view instead */
static void
print_math_tree (EqneditMathElement *element, int indent)
{
#undef PRINTNAME
#define PRINTNAME(child, indent)					\
do {									\
	print_indent (indent);						\
	printf ("%p %s", (child),					\
		gtk_type_name (GTK_OBJECT_TYPE (child)));		\
} while (0)

	PRINTNAME (element, indent);

	if (EQNEDIT_IS_MATH_PRESENTATIONTOKEN (element)) {
		char			*gs;
		gs = eqnedit_math_presentationtoken_get_content
			(EQNEDIT_MATH_PRESENTATIONTOKEN (element));
		printf (" = \"%s\"", gs);
		free (gs);
		printf ("\n");
	} else
	if (EQNEDIT_IS_MATH_FRACTION (element)) {
		printf ("\n");
		print_math_tree (EQNEDIT_MATH_ELEMENT (EQNEDIT_MATH_FRACTION(element)->numerator), indent + 1);
		print_math_tree (EQNEDIT_MATH_ELEMENT (EQNEDIT_MATH_FRACTION(element)->denominator), indent + 1);
	} else
	if (EQNEDIT_IS_MATH_CONTAINER (element)) {
		GList			*child;
		EqneditMathElement	*ochild;

		printf ("\n");

		/* XX create a iterator instead? */
		for (child = EQNEDIT_MATH_CONTAINER(element)->children;
		     child != NULL;
		     child = g_list_next (child)) {
			ochild = child->data;
			print_math_tree (ochild, indent + 1);
		}
	} else {
		/* do nothing */
		printf ("\n");
	}
} /* print_math_tree */
#endif


/* XXX create a view instead */
static void
print_canvasview_tree (EqneditCanvasViewGlyph *glyph, int indent)
{
#undef PRINTNAME
#define PRINTNAME(child, indent)					\
do {									\
	print_indent (indent);						\
	printf ("%p %s (model=%p, group=%p)", ((void *)(child)),	\
		gtk_type_name (GTK_OBJECT_TYPE (child)),		\
		((void *)(MVC_VIEW(child)->observed_model)),		\
		((void *)(child->group)));				\
} while (0)

	PRINTNAME (glyph, indent);

	if (EQNEDIT_IS_CANVASVIEW_TOKEN (glyph)) {
		char			*s;
		gtk_object_get (GTK_OBJECT (EQNEDIT_CANVASVIEW_TOKEN(glyph)->_priv->text), "text", &s, NULL);
		printf (" = \"%s\"", s);
		printf ("\n");
	} else
	if (EQNEDIT_IS_CANVASVIEW_FRACTION (glyph)) {
		printf ("\n");

		print_canvasview_tree (EQNEDIT_CANVASVIEW_GLYPH (EQNEDIT_CANVASVIEW_FRACTION(glyph)->_priv->numerator), indent + 1);
		print_canvasview_tree (EQNEDIT_CANVASVIEW_GLYPH (EQNEDIT_CANVASVIEW_FRACTION(glyph)->_priv->denominator), indent + 1);
	} else
	if (EQNEDIT_IS_CANVASVIEW_CONTAINER (glyph)) {
		GList			*child;
		EqneditCanvasViewGlyph	*ochild;

		printf ("\n");

		for (child = EQNEDIT_CANVASVIEW_CONTAINER(glyph)->children;
		     child != NULL;
		     child = g_list_next (child)) {
			ochild = child->data;
			print_canvasview_tree (ochild, indent + 1);
		}
	} else {
		/* do nothing */
		printf ("\n");
	}
} /* print_canvasview_tree */


/* XXX create a view instead */
static void
print_canvas_tree (GnomeCanvasItem *item, int indent)
{
#undef PRINTNAME
#define PRINTNAME(child, indent)					\
do {									\
	print_indent (indent);						\
	printf ("%p %s", ((void *)(child)),				\
		gtk_type_name (GTK_OBJECT_TYPE (child)));		\
} while (0)

	PRINTNAME (item, indent);

	if (GNOME_IS_CANVAS_TEXT (item)) {
		char			*s;
		gtk_object_get (GTK_OBJECT (item), "text", &s, NULL);
		printf (" = \"%s\"", s);
		printf ("\n");
	} else
	if (GNOME_IS_CANVAS_GROUP (item)) {
		GList			*child;
		GnomeCanvasItem		*ochild;

		printf ("\n");

		/* XX create a iterator instead? */
		for (child = GNOME_CANVAS_GROUP(item)->item_list;
		     child != NULL;
		     child = g_list_next (child)) {
			ochild = child->data;
			print_canvas_tree (ochild, indent + 1);
		}
	} else {
		/* do nothing */
		printf ("\n");
	}
} /* print_canvas_tree */


static gint
delete_event_cb (GtkWidget *window, GdkEventAny *e, gpointer data)
{
	GnomeCanvas	*canvas;

	canvas = GNOME_CANVAS (data);
	printf ("\nEqenditMath tree:\n");
	/*print_math_tree (EQNEDIT_MATH_ELEMENT (eqnedit_root_math_container), 0);*/
	printf ("\nEqneditCanvasView tree:\n");
/*	print_canvasview_tree
		(EQNEDIT_CANVASVIEW_GLYPH (eqnedit_root_canvasview_container),
		 0); */
	printf ("\nGnomeCanvas tree:\n");
	print_canvas_tree (GNOME_CANVAS_ITEM (gnome_canvas_root (canvas)), 0);

	return FALSE;
} /* delete_event_cb */


static char *
cwd (void)
{
	char	s[4096];

	getcwd (s, sizeof (s));
	return g_strdup(s);
} /* cwd */


typedef struct _EqneditConstructData EqneditConstructData;
struct _EqneditConstructData {
	gchar	**start_file_list;
	gint	start_file_list_num;
};


static EqneditConstructData *
eqnedit_construct (int argc, char **argv)
{
	poptContext		ctx;
	const char		**leftovers;
	EqneditConstructData	*d;
	gchar			*current_dir;
	struct poptOption	options[] = {
					{NULL, '\0', 0, NULL, 0}
				};

	d = g_new0 (EqneditConstructData, 1);

	current_dir = cwd ();

	eqnedit_glade_init_with_popt ("Eqnedit", VERSION, argc, argv,
				      options, 0, &ctx);

	d->start_file_list = NULL;
	d->start_file_list_num = 0;
	leftovers = poptGetArgs (ctx);
	if (leftovers) {
		for(;
		    leftovers[d->start_file_list_num];
		    d->start_file_list_num++) {
			d->start_file_list = (gchar **) g_realloc
				(d->start_file_list,
				 (d->start_file_list_num+2)* sizeof(gchar *));

			if (*leftovers[d->start_file_list_num] != '/')
				d->start_file_list[d->start_file_list_num] = g_strconcat (current_dir, "/", leftovers[d->start_file_list_num], NULL);
			else
				d->start_file_list[d->start_file_list_num] = g_strdup (leftovers[d->start_file_list_num]);
		}

		d->start_file_list[d->start_file_list_num] = NULL;
	}
	poptFreeContext(ctx);

	/* XXX g_free (current_dir) */

	return d;
} /* eqnedit_construct */


static void
eqnedit_postconstruct (EqneditDocumentObject *document,
		       EqneditConstructData *d)
{
	gint		i;
	gboolean	success;

	if (d->start_file_list_num > 0) {
		for (i = 0; i < d->start_file_list_num; i++) {
			success = eqnedit_document_object_open
					(document, (d->start_file_list)[i]);

			if (!success) {
				fprintf (stderr, PACKAGE ": Error: "
					 "couldn't open %s\n",
					 (d->start_file_list)[i]);
			}

			g_free ((d->start_file_list)[i]);
		}

		g_free (d->start_file_list);
		g_free (d);
	}
} /* eqnedit_postconstruct */


int
main (int argc, char **argv)
{
	GtkWidget		*win;
	GtkWidget		*canvas;
	GladeXML		*xml;
	EqneditDocumentObject	*document;
	EqneditConstructData	*d;

	d = eqnedit_construct (argc, argv);

	win = eqnedit_glade_get_widget_singleton ("eqnedit.glade", "eqnedit",
						  &xml);
	if (win == NULL)
		return 1;

	canvas = glade_xml_get_widget (xml, "canvas");
	if (canvas == NULL)
		return 1;
	gtk_signal_connect (GTK_OBJECT (win), "delete_event",
			    GTK_SIGNAL_FUNC (delete_event_cb), canvas);

	document = eqnedit_document_object_new (GNOME_CANVAS (canvas),
						GTK_WINDOW (win));

	/* XX uh uh! nice hack. It is used in eqnedit-gui.c  =) */
	gtk_object_set_user_data (GTK_OBJECT (win), document);

	eqnedit_postconstruct (document, d);

	gtk_main ();

	gtk_object_unref (GTK_OBJECT (document));

	gtk_widget_destroy (win);

	return 0;
} /* main */
