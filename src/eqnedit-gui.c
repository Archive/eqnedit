#include <gnome.h>

#include <config-full.h>

#include "my-func.h"
#include "eqnedit-glade.h"
#include "eqnedit-document-object.h"
#include "eqnedit-glade.h"

/* #define UTIL_TRACE */
#include "util-trace.h"
#define __GOB_FUNCTION__    __FILE__



static GtkWidget *about_dialog	= NULL;
static GtkWidget *manual_dialog	= NULL;



/* ============================================================== */
/* eqnedit_dialog                                                 */
/* ============================================================== */


static void
eqnedit_dialog_not_directory (void)
{
	GtkWidget	*dialog;

	TRACEIN;

	dialog = gnome_message_box_new ("Please enter a filename,\n"
					"not a directory.",
					GNOME_MESSAGE_BOX_ERROR,
					GNOME_STOCK_BUTTON_OK, NULL);
	gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
	gtk_widget_show (dialog);

	TRACEOUT;
} /* eqnedit_dialog_not_directory */


static gboolean
eqnedit_dialog_gui_yes_no (const gchar *box_type, const char *msg)
{
	GtkWidget	*dialog;
	gboolean	ret;
	gint		answer;

	TRACEIN;

	dialog = gnome_message_box_new (msg, box_type,
					GNOME_STOCK_BUTTON_YES,
					GNOME_STOCK_BUTTON_NO, NULL);
	answer = gnome_dialog_run (GNOME_DIALOG (dialog));

	switch (answer) {
	case 0: /* Yes */
		ret = TRUE;
		break;
	case 1: /* No */
	case -1: /* delete-event */
		ret = FALSE;
		break;
	default:
		WARNMSG ("Unknown answer from gnome_dialog_run: %d, "
			 "falling down to No", answer);
		ret = FALSE;
		break;
	}

	TRACEOUT;

	return ret;
} /* eqnedit_dialog_gui_yes_no */



/* ============================================================== */
/* About dialog                                                   */
/* ============================================================== */


static gboolean
eqnedit_gui_about_close (GtkWidget *widget, gpointer user_data)
{
	TRACEIN;
	about_dialog = NULL;
	TRACEOUT;
	return FALSE;
} /* eqnedit_gui_about_close */


void eqnedit_gui_about (GtkWidget *widget, gpointer user_data);
void eqnedit_gui_about (GtkWidget *widget, gpointer user_data)
{
	const gchar *copyright = "Copyright 2000 Roberto Arturo Tena S�nchez";
	const gchar *comments = "A mathematical equation editor.";
	const gchar *authors[] = {
		"Arturo Tena <arturo@directmail.org>",
		NULL
	};

	TRACEIN;

	if (about_dialog == NULL) {
		about_dialog = gnome_about_new (PACKAGE, VERSION, copyright,
						(const gchar **) authors,
						comments, NULL);
		gtk_signal_connect (GTK_OBJECT (about_dialog), "close",
				    (GtkSignalFunc) eqnedit_gui_about_close,
				    NULL);
		gtk_widget_show (about_dialog);
	}

	TRACEOUT;
} /* eqnedit_gui_about */



/* ============================================================== */
/* Manual dialog                                                  */
/* ============================================================== */


static gboolean
eqnedit_gui_manual_close (GtkWidget *widget, gpointer user_data)
{
	TRACEIN;
	manual_dialog = NULL;
	TRACEOUT;
	return FALSE;
} /* eqnedit_gui_manual_close */


void eqnedit_gui_manual (GtkWidget *widget, gpointer user_data);
void eqnedit_gui_manual (GtkWidget *widget, gpointer user_data)
{
	const gchar *message =
		"1. Click on an empty rectangle.\n"
		"2. Press letters and numbers.\n"
		"3. To create a fraction, pres key 'f'.\n"
		"4. To create a useless rectangle, press key 'c'.\n"
		"5. Go to step 1.\n"
		"6. When closing, it prints a state of the three\n"
		"   internal trees.\n";

	TRACEIN;

	if (manual_dialog == NULL) {
		manual_dialog = gnome_message_box_new (message,
						       GNOME_MESSAGE_BOX_INFO,
						       GNOME_STOCK_BUTTON_OK,
						       NULL);
		gtk_signal_connect (GTK_OBJECT (manual_dialog), "close",
				    (GtkSignalFunc) eqnedit_gui_manual_close,
				    NULL);


		gtk_widget_show (manual_dialog);
	}

	TRACEOUT;
} /* eqnedit_gui_about */



/* ============================================================== */
/* Simple callbacks                                               */
/* ============================================================== */


/*
void eqnedit_gui_show_widget (GtkWidget *widget, gpointer user_data);
void eqnedit_gui_show_widget (GtkWidget *widget, gpointer user_data)
{
	GtkWidget		*wid;

	g_return_if_fail (user_data != NULL);

	TRACEIN;
	wid = eqnedit_glade_get_widget ("eqnedit.glade", user_data, NULL);
	gtk_widget_show (wid);
	TRACEOUT;
} */ /* eqnedit_gui_show_widget */


void eqnedit_gui_show_widget_singleton (GtkWidget *widget, gpointer user_data);
void eqnedit_gui_show_widget_singleton (GtkWidget *widget, gpointer user_data)
{
	GtkWidget		*wid;

	g_return_if_fail (user_data != NULL);

	TRACEIN;
	wid = eqnedit_glade_get_widget_singleton ("eqnedit.glade", user_data,
						  NULL);
	gtk_widget_show (wid);
	TRACEOUT;
} /* eqnedit_gui_show_widget_singleton */


gboolean
eqnedit_gui_test_quit (GtkWidget *widget, GdkEvent *event, gpointer user_data);
gboolean
eqnedit_gui_test_quit (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	GtkWidget		*top_window;
	EqneditDocumentObject	*document;
	gboolean		stay;

	top_window = eqnedit_glade_get_widget_singleton ("eqnedit.glade",
							 "eqnedit", NULL);
	if (top_window == NULL) { WARNMSG ("top_window == NULL"); }
	document = gtk_object_get_user_data (GTK_OBJECT (top_window));
	if (document == NULL) { WARNMSG ("document == NULL"); }

	if (!eqnedit_document_object_is_dirty (document)) {
		stay = FALSE;
	} else {
		stay = !eqnedit_dialog_gui_yes_no (GNOME_MESSAGE_BOX_WARNING,
						   "The equation has not been "
						   "saved,\nare you sure you "
						   "want to quit?");

	}

	if (!stay)
		gtk_main_quit ();
	return stay;
} /* eqnedit_gui_test_quit */



/* ============================================================== */
/* New callbacks and functions                                    */
/* ============================================================== */


void eqnedit_gui_new (GtkWidget *widget, gpointer user_data);
void eqnedit_gui_new (GtkWidget *widget, gpointer user_data) 
{
	EqneditDocumentObject	*document;
	GtkWidget		*top_window;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_WIDGET (widget));

	TRACEIN;

	top_window = eqnedit_glade_get_widget_singleton ("eqnedit.glade",
							 "eqnedit", NULL);
	if (top_window == NULL) { WARNMSG ("top_window == NULL"); }
	document = gtk_object_get_user_data (GTK_OBJECT (top_window));
	if (document == NULL) { WARNMSG ("document == NULL"); }

	if (!eqnedit_document_object_is_dirty (document)) {
		eqnedit_document_object_clean_as_new (document);
	} else {
		gboolean	sure;

		sure = eqnedit_dialog_gui_yes_no (GNOME_MESSAGE_BOX_WARNING,
						  "The equation has not been "
						  "saved,\nare you sure you "
						  "want to delete it?");
		if (sure)
			eqnedit_document_object_clean_as_new (document);
	}

	TRACEOUT;
} /* eqnedit_gui_new */



/* ============================================================== */
/* File opening callbacks and functions                           */
/* ============================================================== */


static void
try_open (EqneditDocumentObject *document, const char *filename_to_open)
{
	gboolean		success;

	g_return_if_fail (document != NULL);
	g_return_if_fail (EQNEDIT_IS_DOCUMENT_OBJECT (document));

	TRACEIN;

	while (1) {
		success = eqnedit_document_object_open (document,
							filename_to_open);
		if (success) {
			break; /* exit from while (1) loop */
		} else {
			gboolean	loop;

			loop = eqnedit_dialog_gui_yes_no
				(GNOME_MESSAGE_BOX_ERROR,
				 "Error opening equation.\n"
				 "Do you want to try again?");
			if (!loop)
				break; /* exit from while (1) loop */
		}
	} /* try over and over at user's choice */

	TRACEOUT;
} /* try_open */


static gboolean
eqnedit_gui_open_check (GtkFileSelection *filesel, char *filename_to_open)
{
	TRACEIN;
	if (g_file_test (filename_to_open, G_FILE_TEST_ISDIR)) {
		eqnedit_dialog_not_directory ();
		TRACEOUT;
		return FALSE;
	}

	/* Do not check if the file actually exists, it must be handled by
	   EqneditOpener* objects */

	TRACEOUT;
	return TRUE;
} /* eqnedit_gui_open_check */


static void
do_open (EqneditDocumentObject *document)
{
	GtkFileSelection	*file_selector;
	GtkWidget		*file_selector_widget;
	gint			cause;
	char			*filename_to_open;

	g_return_if_fail (document != NULL);
	g_return_if_fail (EQNEDIT_IS_DOCUMENT_OBJECT (document));

	TRACEIN;

	file_selector_widget = gtk_file_selection_new ("Open equation");
	file_selector = GTK_FILE_SELECTION (file_selector_widget);
	/* gtk_file_selection_complete (file_selector, "*.eqnedit"); */
	gtk_file_selection_set_filename (file_selector, "");

	cause = gtk_file_selection_run (file_selector,
					eqnedit_gui_open_check, NULL);
	switch (cause) {
	case 0: /* OK */
		filename_to_open = g_strdup (gtk_file_selection_get_filename
						(file_selector));
		try_open (document, filename_to_open);
		free (filename_to_open);
		break;
	case 1: /* Cancel */
	case 2: /* delete event */
		/* do nothing else */
		break;
	default:
		WARNMSG ("Unknown exit cause from gtk_file_selection_run: %d",
			 cause);
		break;
	}
	
	gtk_widget_destroy (GTK_WIDGET (file_selector));
	/* XXX nor gtk_object_unref neither gtk_widget_unref work,
	   is this a GTK+ bug? */

	TRACEOUT;
} /* do_open */


void
eqnedit_gui_open (GtkWidget *widget, gpointer user_data);
void
eqnedit_gui_open (GtkWidget *widget, gpointer user_data) 
{
	GtkWidget		*top_window;
	EqneditDocumentObject	*document;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_WIDGET (widget));

	TRACEIN;

	top_window = eqnedit_glade_get_widget_singleton ("eqnedit.glade",
							 "eqnedit", NULL);
	if (top_window == NULL) { WARNMSG ("top_window == NULL"); }
	document = gtk_object_get_user_data (GTK_OBJECT (top_window));
	if (document == NULL) { WARNMSG ("document == NULL"); }

	if (!eqnedit_document_object_is_dirty (document)) {
		do_open (document);
	} else {
		gboolean	sure;

		sure = eqnedit_dialog_gui_yes_no (GNOME_MESSAGE_BOX_WARNING,
						  "The equation has not been "
						  "saved,\nare you sure you "
						  "want to discard it?");
		if (sure)
			do_open (document);
	}

	TRACEOUT;
} /* eqnedit_gui_open */



/* ============================================================== */
/* File saving callbacks and functions                            */
/* ============================================================== */


static void
try_save (EqneditDocumentObject *document, const char *filename_to_save)
{
	gboolean	success;

	g_return_if_fail (document != NULL);
	g_return_if_fail (EQNEDIT_IS_DOCUMENT_OBJECT (document));

	TRACEIN;

	while (1) {
		if (filename_to_save != NULL) {
			success = eqnedit_document_object_save_as
					(document, filename_to_save,
					 ".eqnedit");
		} else {
			success = eqnedit_document_object_save (document);
		}
	
		if (success) {
			break; /* exit from while (1) loop */
		} else {
			gboolean	loop;

			loop = eqnedit_dialog_gui_yes_no
				(GNOME_MESSAGE_BOX_ERROR,
				 "Error saving equation.\n"
				 "Do you want to try again?");
			if (!loop)
				break; /* exit from while (1) loop */
		}
	} /* try over and over at user's choice */

	TRACEOUT;
} /* try_save */


static gboolean
eqnedit_gui_save_check (GtkFileSelection *filesel, char *filename_to_save)
{
	char	*basename;

	TRACEIN;
	if (g_file_test (filename_to_save, G_FILE_TEST_ISDIR)) {
		eqnedit_dialog_not_directory ();
		TRACEOUT;
		return FALSE;
	}

	basename = g_basename (filename_to_save);
	if (strchr (basename, '.') == NULL) {
		char *old_filename_to_save;

		old_filename_to_save = filename_to_save;
		filename_to_save = g_strconcat (old_filename_to_save,
						".eqnedit", NULL);
		free (old_filename_to_save);
	}

	if (g_file_exists (filename_to_save)) {
		if (!eqnedit_dialog_gui_yes_no (GNOME_MESSAGE_BOX_WARNING,
						"Equation already exists.\n"
						"Do you want to save over "
						"it?")) {

			return FALSE;
		}
	}

	TRACEOUT;
	return TRUE;
} /* eqnedit_gui_save_check */


static void
do_save_as (EqneditDocumentObject *document)
{
	GtkFileSelection	*file_selector;
	GtkWidget		*file_selector_widget;
	gint			cause;
	char			*filename_to_save;
	const char		*document_filename;

	g_return_if_fail (document != NULL);
	g_return_if_fail (EQNEDIT_IS_DOCUMENT_OBJECT (document));

	TRACEIN;
	
	file_selector_widget = gtk_file_selection_new ("Save equation as");
	file_selector = GTK_FILE_SELECTION (file_selector_widget);
	/* gtk_file_selection_complete (file_selector, "*.eqnedit"); */
	document_filename = eqnedit_document_object_get_filename (document);
	if (document_filename != NULL) {
		gtk_file_selection_set_filename (file_selector,
						 document_filename);
	} else {
		gtk_file_selection_set_filename (file_selector,
						 "Equation1.eqnedit");
	}

	cause = gtk_file_selection_run (file_selector,
					eqnedit_gui_save_check, NULL);
	switch (cause) {
	case 0: /* OK */
		filename_to_save = g_strdup (gtk_file_selection_get_filename
						(file_selector));
		try_save (document, filename_to_save);
		free (filename_to_save);
		break;
	case 1: /* Cancel */
	case 2: /* delete event */
		/* do nothing else */
		break;
	default:
		WARNMSG ("Unknown exit cause from gtk_file_selection_run: %d",
			 cause);
		break;
	}
	
	gtk_widget_destroy (GTK_WIDGET (file_selector));
	/* XXX nor gtk_object_unref neither gtk_widget_unref work,
	   is this a GTK+ bug? */

	TRACEOUT;
} /* do_save_as */


void eqnedit_gui_save_as (GtkWidget *widget, gpointer user_data);
void eqnedit_gui_save_as (GtkWidget *widget, gpointer user_data) 
{
	GtkWidget		*top_window;
	EqneditDocumentObject	*document;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_WIDGET (widget));

	TRACEIN;

	top_window = eqnedit_glade_get_widget_singleton ("eqnedit.glade",
							 "eqnedit", NULL);
	if (top_window == NULL) { WARNMSG ("top_window == NULL"); }
	document = gtk_object_get_user_data (GTK_OBJECT (top_window));
	if (document == NULL) { WARNMSG ("document == NULL"); }

	do_save_as (document);

	TRACEOUT;
} /* eqnedit_gui_save_as */


void eqnedit_gui_save (GtkWidget *widget, gpointer user_data);
void eqnedit_gui_save (GtkWidget *widget, gpointer user_data) 
{
	EqneditDocumentObject	*document;
	GtkWidget		*top_window;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_WIDGET (widget));

	TRACEIN;

	top_window = eqnedit_glade_get_widget_singleton ("eqnedit.glade",
							 "eqnedit", NULL);
	if (top_window == NULL) { WARNMSG ("top_window == NULL"); }
	document = gtk_object_get_user_data (GTK_OBJECT (top_window));
	if (document == NULL) { WARNMSG ("document == NULL"); }

	if (eqnedit_document_object_can_do_save (document))
		try_save (document, NULL);
	else
		do_save_as (document);

	TRACEOUT;
} /* eqnedit_gui_save_as */
