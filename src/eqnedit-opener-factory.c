#include "eqnedit-opener-factory.h"
#include "eqnedit-opener-xml.h"

/* #define UTIL_TRACE */
#include <util-trace.h>
#define __GOB_FUNCTION__    __FILE__


EqneditOpenerObject *
eqnedit_opener_factory_new_opener (const gchar *filename)
{
	EqneditOpenerObject	*ret;

	/* The only opener is XML, since there are no more saver now,
	   when there are we need to test filename type and create the
	   correct opener */

	ret = eqnedit_opener_xml_new (filename);

	return ret;
} /* eqnedit_saver_factory_new_saver */
