/*
 * mvc.h: Header for the thriad Model-View Controller
 *
 * Copyright (c) 2000, Arturo Tena
 *
 * Author: Arturo Tena <arturo@directmail.org>
 *
 */

#ifndef __MVC_H
#define __MVC_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdio.h>

#ifdef HAVE_CONFIG_H
#  include <mvc-config.h>
#endif

#ifdef MVC_ENABLE_DEBUG
#  define MVC_DEBUG 2
#else
#  define MVC_DEBUG 0
#endif

#if MVC_DEBUG > 1
#  define MVC_NOT_HANDLED_EVENT_SIGNAL_WARNING                            \
	g_warning ("Signal %s called on object %p with not handled event" \
		   "(%d, %p)", __FUNCTION__, self, event, user_data)
#  define MVC_CALLED_VIRTUAL_METHOD					  \
	g_warning ("Called virtual method");
#else
#  define MVC_NOT_HANDLED_EVENT_SIGNAL_WARNING ;
#  define MVC_CALLED_VIRTUAL_METHOD ;
#endif

#if MVC_DEBUG > 0
#  define MVC_ENTRY                                                     \
do {                                                                    \
	printf("==> %s (%s, line %d)\n", __FUNCTION__,	                \
	       __FILE__, __LINE__);                                     \
	fflush (stdout);                                                \
} while (0)
#  define MVC_EXIT                                                      \
do {                                                                    \
	printf("<== %s (%s, line %d)\n", __FUNCTION__,	                \
	       __FILE__, __LINE__);                                     \
	fflush (stdout);                                                \
} while (0)
#else
#  define MVC_ENTRY ;
#  define MVC_EXIT ;
#endif

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __MVC_H */
