#ifndef __MVC_CLASSES_H__
#define __MVC_CLASSES_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#include "mvc-controller.h"
#include "mvc-model.h"
#include "mvc-view.h"

	
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __MVC_CLASSES_H_ */
