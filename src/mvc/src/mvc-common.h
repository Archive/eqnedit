/*
 * mvc-common.h: Common code for the thriad Model-View Controller
 *
 * Copyright (c) 2000, Arturo Tena
 *
 * Author: Arturo Tena <arturo@directmail.org>
 *
 */

#ifndef __MVC_COMMON_H
#define __MVC_COMMON_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdio.h>

#include "mvc.h"
	
#define MVC_NONE (/**/)

#define MVC_THE_SIGNAL(CLASSNAME,obj,signal)                            \
	(CLASSNAME(GTK_OBJECT(obj)->klass)->signal)

#define MVC_SIGNAL(CLASSNAME,obj,signal,args)                           \
THIS_DEFINE_IS_DEPRECATED_USE_gtk_signal_emit_by_name_INSTEAD;          \
do {                                                                    \
	if (MVC_THE_SIGNAL(CLASSNAME,obj,signal)) {                     \
		(* MVC_THE_SIGNAL(CLASSNAME,obj,signal)) (args);        \
	}                                                               \
} while (0)

#define MVC_SIGNAL_CLASS(CLASSNAME,classobj,signal,args)                \
do {                                                                    \
	if (CLASSNAME(classobj)->signal) {                              \
		(* CLASSNAME(classobj)->signal) args;                   \
	}                                                               \
} while (0)

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __MVC_COMMON_H */
