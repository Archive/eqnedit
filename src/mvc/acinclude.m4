dnl *********************************************************************
dnl GOB_CHECK

dnl GOB_HOOK(script if found, fail)
dnl if fail = "failure", abort if GOB not found
dnl


AC_DEFUN([GOB_HOOK],[
	AC_PATH_PROG(GOB,gob)
	if test ! x$GOB = x; then	
		if test ! x$1 = x; then 
			AC_MSG_CHECKING(for gob >= $1)
			g_r_ve=`echo $1|sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\1/'`
			g_r_ma=`echo $1|sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\2/'`
			g_r_mi=`echo $1|sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`
			g_ve=`$GOB --version 2>&1|sed 's/Gob version \([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\1/'`
			g_ma=`$GOB --version 2>&1|sed 's/Gob version \([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\2/'`
			g_mi=`$GOB --version 2>&1|sed 's/Gob version \([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`

			if test $g_ve -eq $g_r_ve; then
				if test $g_ma -ge $g_r_ma; then
					if test $g_mi -ge $g_r_mi; then
						AC_MSG_RESULT(ok)
					else
						if test $g_ma -gt $g_r_ma; then
							AC_MSG_RESULT(ok)
						else
							AC_MSG_ERROR("found $g_ve.$g_ma.$g_mi requires $g_r_ve.$g_r_ma.$g_r_mi")
						fi
					fi
				else
					AC_MSG_ERROR("found $g_ve.$g_ma.$g_mi requires $g_r_ve.$g_r_ma.$g_r_mi")
				fi
			else
				if test $g_ve -gt $g_r_ve; then
					AC_MSG_RESULT(ok)
				else
					AC_MSG_ERROR(major version $g_ve found but $g_r_ve required)
				fi
			fi
	
			unset gob_version
			unset g_ve
			unset g_ma
			unset g_mi
			unset g_r_ve
			unset g_r_ma
			unset g_r_mi
		fi
		AC_SUBST(GOB)
		$2
	else		
		AC_MSG_ERROR([Cannot find GOB, check http://www.5z.com/jirka/gob.html])
		$3
	fi
])

AC_DEFUN([GOB_CHECK],[
	GOB_HOOK($1,[],[])
])


dnl *********************************************************************
dnl COMPILE_WARNINGS
dnl Turn on many useful compiler warnings
dnl For now, only works on GCC

AC_DEFUN([COMPILE_WARNINGS],[

  enableval=no
  AC_ARG_ENABLE(compile-warnings, 
    [  --enable-compile-warnings     Turn on compiler warnings])
  AC_MSG_CHECKING(what warning flags to pass to the C compiler)
  warnCFLAGS=
  if test "x$enableval" = "xyes"; then
    if test "x$GCC" = "xyes"; then
      warnCFLAGS="-Wall -Wunused -Wmissing-prototypes -Wmissing-declarations -Wpointer-arith -Wsign-compare -Wbad-function-cast -Wconversion"
    fi
  fi
  AC_MSG_RESULT($warnCFLAGS)


  enableval=no
  AC_ARG_ENABLE(iso-c,
    [  --enable-iso-c          Try to warn if code is not ISO C ])
  AC_MSG_CHECKING(what language compliance flags to pass to the C compiler)
  complCFLAGS=
  if test "x$enableval" = "xyes"; then
    if test "x$GCC" = "xyes"; then
        complCFLAGS="-ansi -pedantic-errors"
    fi
  fi
  AC_MSG_RESULT($complCFLAGS)


  if test "x$compile_warnings_set" != "xyes"; then
    CFLAGS="$CFLAGS $warnCFLAGS $complCFLAGS"
    compile_warnings_set=yes
    AC_SUBST(compile_warnings_set)
  fi

])


dnl *********************************************************************
dnl *********************************************************************
dnl COMPILE_DEBUG
dnl Turn on many userful debug compiler flags
dnl For now, only works on GCC

AC_DEFUN([COMPILE_DEBUG],[

  enableval=no
  AC_ARG_ENABLE(debug,
    [  --enable-debug          Enable compiler debug flags])
  AC_MSG_CHECKING(what debug flags to pass to the C compiler)
  debugCFLAGS=
  if test "x$enableval" = "xyes"; then
    if test "x$GCC" = "xyes"; then
      debugCFLAGS="$debugCFLAGS -g -O0 -Werror"
    fi
  fi
  AC_MSG_RESULT($debugCFLAGS)


  if test "x$compile_debug_set" != "xyes"; then
    CFLAGS="$CFLAGS $debugCFLAGS"
    compile_debug_set=yes
    AC_SUBST(compile_debug_set)
  fi

])


dnl *********************************************************************
dnl *********************************************************************
dnl UTIL_TRACE
dnl Enable util trace

AC_DEFUN([UTIL_TRACE],[

  enableval=no
  AC_ARG_ENABLE(util-trace,
    [  --enable-util-trace     Enable util trace])
  AC_MSG_CHECKING(what trace flags to pass to the C compiler)
  traceCFLAGS=
  if test "x$enableval" = "xyes"; then
    traceCFLAGS="$traceCFLAGS -DUTIL_TRACE"
  fi
  AC_MSG_RESULT($traceCFLAGS)


  if test "x$util_trace_set" != "xyes"; then
    CFLAGS="$CFLAGS $traceCFLAGS"
    util_trace_set=yes
    AC_SUBST(util_trace_set)
  fi

])


dnl *********************************************************************
