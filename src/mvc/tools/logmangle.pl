#!/usr/bin/perl

# Copied from Achtung

use IO::Handle;

my $logfile = shift;

open FH, "$logfile";

STDOUT->autoflush(1);

my %colours = ("==> " => "\033[1;m",
	       "<== " => "\033[31;m");

my $nest = "";

while ( <FH> ) {
    my $k;
    my $foo = 0;

    chop $_;

    if (index($_, "==> ") == 0) {
	print $nest . %colours->{"==> "} . $_ . "\033[0;m\n";
	$nest .= "  ";
	next;
    } elsif (index($_, "<== ") == 0) {
	$nest = substr($nest, 2);
	print $nest . %colours->{"<== "} . $_ . "\033[0;m\n";
	next;
    }
    print "$_\n";
}
