#include "mvc3-model.h"
#include "mvc3-view1.h"
#include "mvc3-view2.h"
#include "mvc3-controller1.h"

static gint delete_event_cb (GtkWidget* window, GdkEventAny* e, gpointer data);

int
main (int argc, char **argv)
{
	GtkObject *model;
	GtkObject *view1;
	GtkObject *view2;
	GtkObject *controller1;
	GtkWidget *window;

	gtk_init (&argc, &argv);

	model = mvc3_model_new ();
	
	view1 = mvc3_view1_new ();
	mvc_view_observe_model (MVC_VIEW (view1), MVC_MODEL (model),
				MVC_EVENT_TICK);
	controller1 = mvc3_controller1_new ();
	mvc_controller_set_dependence (MVC_CONTROLLER (controller1),
				       MVC_MODEL (model));
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size (GTK_WINDOW (window), 300, 49);
	gtk_signal_connect(GTK_OBJECT(window), "delete_event",
			   GTK_SIGNAL_FUNC (delete_event_cb), NULL);
	gtk_container_add (GTK_CONTAINER (window),
			   mvc3_controller1_get_gtk_widget
			   	(MVC3_CONTROLLER1 (controller1)));
	gtk_widget_show_all (window);

	view2 = mvc3_view2_new ();
	mvc_view_observe_model (MVC_VIEW (view2), MVC_MODEL (model),
				MVC_EVENT_TICK);

	
	g_message ("Live and let die...");
	gtk_main ();

	g_message ("The model has dead, long live the model...");

	gtk_object_unref (model);
	gtk_object_unref (view1);
	gtk_object_unref (view2);
	gtk_object_unref (controller1);

	return 0;
}

gint 
delete_event_cb (GtkWidget* window, GdkEventAny* e, gpointer data)
{
	gtk_main_quit ();

	return FALSE;
}
