#include "mvc-model.h"
#include "mvc-view.h"
#include "mvc-controller.h"

int
main (int argc, char **argv)
{
	GtkObject *model;
	GtkObject *view1;
	GtkObject *view2;
	GtkObject *controller1;
	GtkObject *controller2;

	gtk_init (&argc, &argv);

	model = mvc_model_new ();

	view1 = mvc_view_new ();
	mvc_view_observe_model (MVC_VIEW (view1), MVC_MODEL (model),
				MVC_EVENT_DESTROY);
	controller1 = mvc_controller_new ();
	mvc_controller_set_dependence (MVC_CONTROLLER (controller1),
				       MVC_MODEL (model));

	view2 = mvc_view_new ();
	mvc_view_observe_model (MVC_VIEW (view2), MVC_MODEL (model),
				MVC_EVENT_DESTROY);
	controller2 = mvc_controller_new ();
	mvc_controller_set_dependence (MVC_CONTROLLER (controller2),
				       MVC_MODEL (model));

	gtk_object_unref (model);
	/*
	gtk_object_unref (view1);
	gtk_object_unref (view2);
	gtk_object_unref (controller1);
	gtk_object_unref (controller2);
	*/

	return 0;
}
