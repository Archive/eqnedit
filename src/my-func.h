/*
 * This are calls which are needed and should exists,
 * but aren't for some reason they're not.
 */

#ifndef __MY_FUNC_H__
#define __MY_FUNC_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

	
#include <gtk/gtk.h>


/* ============================================================== */
/* GtkWidget                                                      */
/* ============================================================== */

#if 0
GtkWidget	*gtk_widget_get_top_gtkwindow	(GtkWidget *widget);
#endif


/* ============================================================== */
/* GtkFileSelection                                               */
/* ============================================================== */

gint gtk_file_selection_run
	(GtkFileSelection * filesel,
	 gboolean (*checkfunc) (GtkFileSelection * filesel, char *filename),
	 void     (*notokfunc) (GtkFileSelection * filesel, char *filename));


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __MY_FUNC__ */
