#include <config-full.h>

#include "eqnedit-saver-factory.h"

#include "eqnedit-saver-xml.h"

/* #define UTIL_TRACE */
#include <util-trace.h>
#define __GOB_FUNCTION__    __FILE__


EqneditSaverObject *
eqnedit_saver_factory_new_saver (const gchar *filename, const gchar *type)
{
	EqneditSaverObject	*ret;

	g_return_val_if_fail (filename != NULL, NULL);
	g_return_val_if_fail (type != NULL, NULL);

	ret = NULL;

	if (!strcmp (type, ".eqnedit")) {
		ret = eqnedit_saver_xml_new (filename);
	} else {
		WARNMSG ("Unknown saver type");
	}

	return ret;
} /* eqnedit_saver_factory_new_saver */
