#ifndef __CONFIG_FULL_H__
#define __CONFIG_FULL_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#include <config.h>


#ifdef HAVE_STRING_H
#  include <string.h>
#endif

#include <stdlib.h>

#ifdef STDC_HEADERS
#  ifdef HAVE_CTYPE_H
#    include <ctype.h>
#  endif
#endif

#if 0
#ifndef HAVE_STRCMP
   int strcmp (const char *s1, const char *s2);
#else
#  ifdef HAVE_STRING_H
#    include <string.h>
#  endif
#endif
#endif

#ifdef HAVE_POPT_H
#  include <popt.h>
#endif


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __CONFIG_FULL_H__ */
