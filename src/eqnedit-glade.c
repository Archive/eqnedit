#include <gnome.h>

#include "eqnedit-glade.h"

/* #define UTIL_TRACE */
#include "util-trace.h"
#define __GOB_FUNCTION__    __FILE__


void
eqnedit_glade_init (char *package, char *version, int argc, char **argv)
{
	g_return_if_fail (package != NULL);
	g_return_if_fail (version != NULL);
	g_return_if_fail (argv != NULL);

	TRACEIN;
	gnome_init (package, version, argc, argv);
	glade_gnome_init ();
	TRACEOUT;
} /* eqnedit_glade_init */


#ifdef HAVE_POPT_H
void
eqnedit_glade_init_with_popt (char *package, char *version, int argc,
			      char **argv, const struct poptOption *options,
			      int flags, poptContext *return_ctx)
{
	g_return_if_fail (package != NULL);
	g_return_if_fail (version != NULL);
	g_return_if_fail (argv != NULL);

	TRACEIN;
	gnome_init_with_popt_table (package, version, argc, argv,
				    options, flags, return_ctx);
	glade_gnome_init ();
	TRACEOUT;
} /* eqnedit_glade_init_with_popt */
#endif



GtkWidget *
eqnedit_glade_get_widget (char *filename, char *widget_name, GladeXML **pxml)
{
	GtkWidget	*widget;
	GladeXML	*xml;

	g_return_val_if_fail (filename != NULL, NULL);
	g_return_val_if_fail (widget_name != NULL, NULL);

	TRACEIN;
	xml = NULL;
	if (g_file_exists (filename)) {
		/* local glade file */
		xml = glade_xml_new (filename, widget_name);
	} else {
		char	*full_filename;

		full_filename = g_strdup_printf ("%s%s", EQNEDIT_GLADEDIR,
						 filename);
		if (g_file_exists (full_filename)) {
			/* installed glade file */
			xml = glade_xml_new (full_filename, widget_name);
		} else {
			WARNMSG ("Can't find %s", full_filename);
		}
		free (full_filename);
	}
	if (pxml != NULL)
		*pxml = xml;

	if (xml == NULL) {
		WARNMSG ("Error creating the interface using libglade");
		return NULL;
	}

	glade_xml_signal_autoconnect (xml);

	widget = glade_xml_get_widget (xml, widget_name);
	if (widget == NULL) {
		WARNMSG ("couldn't find widget %s in %s", widget_name,
			 filename);
	}

	TRACEOUT;
	return widget;
} /* eqnedit_glade_get_widget */


GtkWidget *
eqnedit_glade_get_widget_singleton (char *filename, char *widget_name,
				    GladeXML **pxml)
{
	static GHashTable	*singletons = NULL;
	char			*full_name;
	GtkWidget		*wid;

	g_return_val_if_fail (filename != NULL, NULL);
	g_return_val_if_fail (widget_name != NULL, NULL);

	TRACEIN;
	if (singletons == NULL) {
		singletons = g_hash_table_new (g_str_hash, g_str_equal);
	}

	full_name = g_strdup_printf ("%s:%s", filename, widget_name);
	wid = g_hash_table_lookup (singletons, full_name);
	if (wid == NULL) {
		wid = eqnedit_glade_get_widget (filename, widget_name, pxml);
		if (wid == NULL) {
			TRACEOUT;
			return NULL;
		}
		g_hash_table_insert (singletons, full_name, wid);
	} else {
		free (full_name);
	}

	TRACEOUT;
	return wid;
} /* eqnedit_glade_get_widget_singleton */
