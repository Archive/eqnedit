#ifndef __EQNEDIT_SAVER_FACTORY_H__
#define __EQNEDIT_SAVER_FACTORY_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#include "eqnedit-saver-object.h"
#include "eqnedit-document-object.h"


EqneditSaverObject *eqnedit_saver_factory_new_saver (const gchar *filename,
						     const gchar *type);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __EQNEDIT_SAVER_FACTORY_H__ */
