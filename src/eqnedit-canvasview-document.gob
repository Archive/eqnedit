requires 1.0.3

%alltop{
/* ============================================================== */
/* This will be on top of all output files */

/* ============================================================== */
%}

%headertop{
/* ============================================================== */
/* This will be on top of the public header */

/* ============================================================== */
%}

%privateheader{
/* ============================================================== */
/* This will go into the private header file */

/* ============================================================== */
%}

%h{
/* ============================================================== */
/* This will be included in the header */

#include <gnome.h>

#include <mvc-classes.h>

#ifndef __TYPEDEF_EQNEDIT_DOCUMENT_OBJECT__
#define __TYPEDEF_EQNEDIT_DOCUMENT_OBJECT__
typedef struct _EqneditDocumentObject EqneditDocumentObject;
#endif

#ifndef __TYPEDEF_EQNEDIT_MATH_DOCUMENT__
#define __TYPEDEF_EQNEDIT_MATH_DOCUMENT__
typedef struct _EqneditMathDocument EqneditMathDocument;
#endif

#ifndef __TYPEDEF_EQNEDIT_MATH_CONTAINER__
#define __TYPEDEF_EQNEDIT_MATH_CONTAINER__
typedef struct _EqneditMathContainer EqneditMathContainer;
#endif

#ifndef __TYPEDEF_EQNEDIT_CANVASVIEW_RENDER__
#define __TYPEDEF_EQNEDIT_CANVASVIEW_RENDER__
typedef struct _EqneditCanvasViewRender EqneditCanvasViewRender;
#endif

#ifndef __TYPEDEF_EQNEDIT_CANVASVIEW_CONTAINER__
#define __TYPEDEF_EQNEDIT_CANVASVIEW_CONTAINER__
typedef struct _EqneditCanvasViewContainer EqneditCanvasViewContainer;
#endif

#ifndef __TYPEDEF_EQNEDIT_CANVASVIEW_CURSOR__
#define __TYPEDEF_EQNEDIT_CANVASVIEW_CURSOR__
typedef struct _EqneditCanvasViewCursor EqneditCanvasViewCursor;
#endif

/* ============================================================== */
%}

%a{
/* ============================================================== */
/* This will be included in all files */

/* ============================================================== */
%}

%{
/* ============================================================== */
/* This will be included in the C file */

/* #define UTIL_TRACE */
#include <util-trace.h>

#include "eqnedit-document-object.h"
#include "eqnedit-math-container.h"
#include "eqnedit-math-document.h"
#include "eqnedit-canvasview-render.h"
#include "eqnedit-canvasview-container.h"
#include "eqnedit-canvasview-cursor.h"

/* ============================================================== */
%}

class Eqnedit:CanvasView:Document from MVC:View
{
/*
 * Data members
 */
private	EqneditDocumentObject		*document		= NULL;
private	EqneditCanvasViewContainer	*root_container		= NULL;
public	EqneditCanvasViewCursor		*cursor			= NULL;


/*
 * Arguments
 */


/*
 * Initialization and finalization methods
 */


override (Gtk:Object) void
destroy (Gtk:Object *self_object (check null type))
{
	Self			*self;

	TRACEIN;

	self = SELF (self_object);

	gtk_object_unref (GTK_OBJECT (self->_priv->root_container));
	self->_priv->root_container = NULL;

	PARENT_HANDLER (self_object);

	TRACEOUT;
} /* destroy */


/*
 * Construction methods
 */


public EqneditCanvasViewDocument *
new (const Eqnedit:Math:Document *model (check null type),
     Gnome:Canvas:Group *group (check null type),
     Eqnedit:CanvasView:Render *render (check null type),
     GnomeCanvas *canvas, gboolean autoupdate,
     Eqnedit:Document:Object *document (check null type))
{
	Self	*ret;

	g_return_val_if_fail (!(canvas == NULL && autoupdate), NULL);

	TRACEIN;

	ret = GET_NEW;

	construct (ret, model, group, render, canvas, autoupdate, document);

	TRACEOUT;

	return ret;
} /* new */


protected void
construct (self,
	   const Eqnedit:Math:Document *model (check null type),
	   Gnome:Canvas:Group *group (check type),
	   Eqnedit:CanvasView:Render *render (check null type),
	   GnomeCanvas *canvas, gboolean autoupdate,
	   Eqnedit:Document:Object *document (check null type))
{
	g_return_if_fail (!(canvas == NULL && autoupdate));

	TRACEIN;

	mvc_view_construct (MVC_VIEW (self), MVC_MODEL (model),
			    MVC_EVENT_DESTROY);

	self->_priv->document = document;

	self->_priv->root_container
		= eqnedit_canvasview_container_new_without_parent
			(model->root_container, group, render, canvas, FALSE,
			 self);

#if 1
	self->cursor
		= eqnedit_canvasview_cursor_new
			(EQNEDIT_CANVASVIEW_GLYPH
				 (self->_priv->root_container), render);
	eqnedit_canvasview_cursor_grab_focus (self->cursor);
	eqnedit_canvasview_cursor_refresh (self->cursor);
#endif

	mvc_view_post_construct (MVC_VIEW (self));

	TRACEOUT;
} /* construct */


override (MVC:View) void
create_current_view (MVC:View *self_view (check null type))
{
	TRACEIN;

	/* XXXMSG ("Is necessary to do something here?"); */

	TRACEOUT;
} /* create_current_view */


/*
 * Document interface methods
 */


public void
set_dirty (self)
{
	TRACEIN;

	eqnedit_document_object_set_dirty (self->_priv->document);

	TRACEOUT;
} /* set dirty */


public void
grab_focus (self)
{
	TRACEIN;

	eqnedit_canvasview_cursor_grab_focus (self->cursor);

	TRACEOUT;
} /* grab_focus */


} /* Eqnedit:CanvasView:Document */
