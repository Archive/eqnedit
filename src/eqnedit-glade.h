#ifndef __EQNEDIT_GLADE_H__
#define __EQNEDIT_GLADE_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#include <gtk/gtk.h>
#include <glade/glade.h>
#include <config-full.h>


void		eqnedit_glade_init		(char *package, char *version,
						 int argc, char **argv);

#ifdef HAVE_POPT_H
void		eqnedit_glade_init_with_popt	(char *package, char *version,
						 int argc, char **argv,
						  const struct
						  poptOption *options,
						  int flags,
						  poptContext *return_ctx);
#endif

GtkWidget *	eqnedit_glade_get_widget	(char *filename,
						 char *widget_name,
						 GladeXML **pxml);

GtkWidget *	eqnedit_glade_get_widget_singleton (char *filename,
						    char *widget_name,
						    GladeXML **pxml);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __EQNEDIT_GLADE_H__ */
