#include <gdk/gdkkeysyms.h>

#include "my-func.h"

/* #define UTIL_TRACE */
#include "util-trace.h"
#define __GOB_FUNCTION__    __FILE__



/* ============================================================== */
/* GtkWidget                                                      */
/* ============================================================== */


#if 0
GtkWidget *gtk_widget_get_top_gtkwindow (GtkWidget *widget)
{
	GtkWidget	*ret;

	g_return_val_if_fail (widget != NULL, NULL);
	g_return_val_if_fail (GTK_IS_WIDGET (widget), NULL);

	TRACEIN;

	ret = widget;

	/* XXX Which is the Right Way (R) to do this, call which function? */
	for (ret = widget; ret->parent != NULL; ret = ret->parent)
		/* do nothing else */;

	TRACEOUT;

	return ret;
} /* gtk_widget_get_top_gtkwindow */
#endif



/* ============================================================== */
/* GtkFileSelection                                               */
/* ============================================================== */


/*
gboolean (*FilenameCheckFunc) (GtkFileSelection *filesel, char *filename);
void     (*FilenameNotOkFunc) (GtkFileSelection *filesel, char *filename);
*/


struct GtkFileSelectionRunInfo
{
  GtkFileSelection *filesel;
  gboolean (*checkfunc) (GtkFileSelection * filesel, char *filename);
  void (*notokfunc) (GtkFileSelection * filesel, char *filename);
  guint ok_clicked_id;
  guint cancel_clicked_id;
  guint delete_event_id;
  guint kpress_event_id;
  gint cause;
};


static void
gtk_file_selection_run_ok_clicked (GtkButton * button,
				  struct GtkFileSelectionRunInfo *ri)
{
  char *filename;
  gboolean ok;

  filename = gtk_file_selection_get_filename (ri->filesel);
  if (ri->checkfunc != NULL)
    {
      ok = ((*ri->checkfunc) (ri->filesel, filename));
      if (ok)
        {
          gtk_widget_hide (GTK_WIDGET (ri->filesel));
          ri->cause = 0;
          gtk_main_quit ();
        }
      else
        {
          if (ri->notokfunc != NULL)
            {
              ((*ri->notokfunc) (ri->filesel, filename));
            }
        }
    }
}

static void
gtk_file_selection_run_cancel_clicked (GtkButton * button,
				      struct GtkFileSelectionRunInfo *ri)
{
  gtk_widget_hide (GTK_WIDGET (ri->filesel));
  ri->cause = 1;
  gtk_main_quit ();
}

static gboolean
gtk_file_selection_run_delete_event (GtkWidget * widget, GdkEvent * event,
				    struct GtkFileSelectionRunInfo *ri)
{
  gtk_widget_hide (GTK_WIDGET (ri->filesel));
  ri->cause = 2;
  gtk_main_quit ();
  return TRUE;
}

static gint
gtk_file_selection_run_kpress_event (GtkFileSelection *filesel,
                                     GdkEventKey *event)
{
  if (event->keyval == GDK_Escape)
    {
      gtk_button_clicked (GTK_BUTTON (filesel->cancel_button));
      return 1;
    }
  else
    {
      return 0;
    }
}

gint
gtk_file_selection_run (GtkFileSelection * filesel,
		       gboolean (*checkfunc) (GtkFileSelection * filesel,
					      char *filename),
		       void (*notokfunc) (GtkFileSelection * filesel,
					  char *filename))
{
  struct GtkFileSelectionRunInfo ri = { NULL, NULL, NULL, 0, 0, 0, 0, 0 };
  GtkButton *bok;
  GtkButton *bcancel;
  gboolean was_modal;
  GtkAccelGroup *accel_group;

  bok = GTK_BUTTON (filesel->ok_button);
  bcancel = GTK_BUTTON (filesel->cancel_button);
  was_modal = GTK_WINDOW (filesel)->modal;

  if (!was_modal)
    gtk_window_set_modal (GTK_WINDOW (filesel), TRUE);

  ri.filesel = filesel;
  ri.checkfunc = checkfunc;
  ri.notokfunc = notokfunc;
  ri.ok_clicked_id =
    gtk_signal_connect (GTK_OBJECT (bok), "clicked",
			GTK_SIGNAL_FUNC (gtk_file_selection_run_ok_clicked),
			&ri);
  ri.cancel_clicked_id =
    gtk_signal_connect (GTK_OBJECT (bcancel), "clicked",
			GTK_SIGNAL_FUNC
			(gtk_file_selection_run_cancel_clicked), &ri);
  ri.delete_event_id =
    gtk_signal_connect (GTK_OBJECT (filesel), "delete_event",
			GTK_SIGNAL_FUNC (gtk_file_selection_run_delete_event),
			&ri);
  ri.kpress_event_id =
    gtk_signal_connect (GTK_OBJECT (filesel), "key_press_event",
                        GTK_SIGNAL_FUNC (gtk_file_selection_run_kpress_event),
		        NULL);

  accel_group = gtk_accel_group_new ();
  gtk_widget_add_accelerator (GTK_WIDGET (bcancel), "clicked", accel_group,
			      GDK_Escape, 0, GTK_ACCEL_VISIBLE);
  gtk_window_add_accel_group (GTK_WINDOW (filesel), accel_group);

  if (!GTK_WIDGET_VISIBLE (GTK_WIDGET (filesel)))
    gtk_widget_show (GTK_WIDGET (filesel));

  gtk_main ();

  if (!was_modal)
    gtk_window_set_modal (GTK_WINDOW (filesel), FALSE);

  gtk_signal_disconnect (GTK_OBJECT (bok), ri.ok_clicked_id);
  gtk_signal_disconnect (GTK_OBJECT (bcancel), ri.cancel_clicked_id);
  gtk_signal_disconnect (GTK_OBJECT (filesel), ri.delete_event_id);
  gtk_window_remove_accel_group (GTK_WINDOW (filesel), accel_group);

  return ri.cause;
}


/*
 * Example of use
 */

#if 0
gboolean
filename_check (GtkFileSelection * filesel, char *filename)
{
  static i = 3;

  g_message ("Checking: %s", filename);

  return --i == 0;	/* The third try it returns TRUE */
}

void
filename_notok (GtkFileSelection * filesel, char *filename)
{
  g_message ("Not ok: %s", filename);
}

int
main (int argc, char **argv)
{
  GtkWidget *filesel;
  gint cause;
  char *filename;

  gtk_init (&argc, &argv);

  filesel = gtk_file_selection_new ("Select file");

  cause = gtk_file_selection_run (GTK_FILE_SELECTION (filesel), filename_check,
				 filename_notok);

  filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (filesel));
  g_message ("Ended because %d, choosed %s", cause, filename);

  gtk_widget_destroy (GTK_OBJECT (filesel));
  /* XXX nor gtk_object_unref neither gtk_widget_unref work,
     is this a GTK+ bug? */

  return 0;
}
#endif
