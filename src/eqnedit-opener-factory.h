#ifndef __EQNEDIT_OPENER_FACTORY_H__
#define __EQNEDIT_OPENER_FACTORY_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#include "eqnedit-opener-object.h"


EqneditOpenerObject *eqnedit_opener_factory_new_opener (const gchar *filename);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __EQNEDIT_OPENER_FACTORY_H__ */
