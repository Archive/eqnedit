#ifndef __XML_UTIL_H__
#define __XML_UTIL_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* libxml (aka gnome-xml) stuff */
#include <tree.h>
#include <parser.h>

/* The following is true while we are not using UNICODE */
#define CHAR_XMLCHAR(s) ((xmlChar *)(s))
#define XMLCHAR_CHAR(s) ((char *)(s))

#define MATHML_XML_NS							\
	"http://www.w3.org/1998/Math/MathML"
#define EQNEDIT_XML_NS_1_0						\
	"http://www.gnome.org/projects/eqnedit/schema_eqnedit/v1_0"


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __XML_UTIL_H__ */
