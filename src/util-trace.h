#ifndef __UTIL_TRACE_H__
#define __UTIL_TRACE_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#include <stdio.h>
#include <glib.h>


/*
 * TRACEIN, TRACEOUT
 */

#ifdef UTIL_TRACE
#	define TRACEIN							\
do {									\
	printf ("==> %s\n", __GOB_FUNCTION__);				\
	fflush (stdout);						\
} while (0)
#	define TRACEOUT							\
do {									\
	printf ("<== %s\n", __GOB_FUNCTION__);				\
	fflush (stdout);						\
} while (0)
#else /* ifndef UTIL_TRACE */
#	define TRACEIN  do { ; } while (0)
#	define TRACEOUT do { ; } while (0)
#endif /* ifdef UTIL_TRACE */


/*
 * TRACEMSG, XXXMSG, WARNMSG
 */

#if defined(__GNUC__) && !defined(__STRICT_ANSI__)

#define TRACEMSG(format, args...)					\
do {									\
	g_message ("TRACE (" __GOB_FUNCTION__ ") " format, ##args);	\
} while (0)
#define XXXMSG(format, args...)						\
do {									\
	g_message ("XXX (" __GOB_FUNCTION__ ") " format, ##args);	\
} while (0)
#define WARNMSG(format, args...)					\
do {									\
	g_warning ("(" __GOB_FUNCTION__ ") " format, ##args);		\
} while (0)
	
#else /* __GNUC__ && !__STRICT_ANSI__ */

#define TRACEMSG  ___my_gob_function___=__GOB_FUNCTION__;___actual_TRACEMSG
#define XXXMSG    ___my_gob_function___=__GOB_FUNCTION__;___actual_XXXMSG
#define WARNMSG   ___my_gob_function___=__GOB_FUNCTION__;___actual_WARNMSG
static char *___my_gob_function___ = NULL;
static void ___actual_TRACEMSG (const char *format, ...)
{
	va_list args;
	GString *msg;
	va_start (args, format);
	msg = g_string_new (NULL);
	g_string_sprintfa (msg, "TRACE (%s) %s", ___my_gob_function___, format);
	g_logv (G_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE, msg->str, args);
	g_string_free (msg, TRUE);
	va_end (args);
}
static void ___actual_XXXMSG (const char *format, ...)
{
	va_list args;
	GString *msg;
	va_start (args, format);
	msg = g_string_new (NULL);
	g_string_sprintfa (msg, "XXX (%s) %s", ___my_gob_function___, format);
	g_logv (G_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE, msg->str, args);
	g_string_free (msg, TRUE);
	va_end (args);
}
static void ___actual_WARNMSG (const char *format, ...)
{
	va_list args;
	GString *msg;
	va_start (args, format);
	msg = g_string_new (NULL);
	g_string_sprintfa (msg, "(%s) %s", ___my_gob_function___, format);
	g_logv (G_LOG_DOMAIN, G_LOG_LEVEL_WARNING, msg->str, args);
	g_string_free (msg, TRUE);
	va_end (args);
}
#endif /* __GNUC__ && !__STRICT_ANSI__ */

#if (!defined __GNUC__) || (defined __GNUC__ && defined __STRICT_ANSI__)
static void ___util_trace_h_really_bad_hack_to_avoid_warnings (void)
{
#define __GOB_FUNCTION__ "___util_trace_h_really_bad_hack_to_avoid_warnings"
	((void (*)(void))___actual_TRACEMSG)();
	((void (*)(void))___actual_XXXMSG)();
	((void (*)(void))___actual_WARNMSG)();
	___util_trace_h_really_bad_hack_to_avoid_warnings ();
#undef __GOB_FUNCTION__
}
#endif /* !__GNUC__ || (__GNUC__ && __STRICT_ANSI__) */


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __UTIL_TRACE_H__ */
