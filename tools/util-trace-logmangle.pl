#!/usr/bin/perl

# Copied without shame from Achtung
# Modified by Arturo Tena: take stdin (I'm not a Perl guru, as evident =)
# Modified by Arturo Tena: print level foo (I'm not a Perl guru, as evident =)

use IO::Handle;

my $logfile = shift;

if ($logfile eq "") {

	STDOUT->autoflush(1);
	
#	my %colours = ("==> " => "\033[1;m",
#		       "<== " => "\033[31;m");
	my %colours = ("==> " => "",
		       "<== " => "");
	
	my $nest = "";
	
	while ( <STDIN> ) {
	    my $k;
	    my $foo = 0;
	
	    chop $_;
	
	    if (index($_, "==> ") == 0) {
		$foo = length ($nest);
		$foo = $foo / 2;
		printf ("%2d ", $foo);
		print $nest . %colours->{"==> "} . $_ . "\n"; # . "\033[0;m\n";
		$nest .= "  ";
		next;
	    } elsif (index($_, "<== ") == 0) {
		$nest = substr($nest, 2);
		$foo = length ($nest);
		$foo = $foo / 2;
		printf ("%2d ", $foo);
		print $nest . %colours->{"<== "} . $_ . "\n";  # . "\033[0;m\n";
		next;
	    }
	    print "$_\n";
	}

} else {

	open FH, "$logfile";
	
	STDOUT->autoflush(1);
	
#	my %colours = ("==> " => "\033[1;m",
#		       "<== " => "\033[31;m");
	my %colours = ("==> " => "",
		       "<== " => "");
	
	my $nest = "";
	
	while ( <FH> ) {
	    my $k;
	    my $foo = 0;
	
	    chop $_;
	
	    if (index($_, "==> ") == 0) {
		$foo = length ($nest);
		$foo = $foo / 2;
		printf ("%2d ", $foo);
		print $nest . %colours->{"==> "} . $_ . "\n"; # "\033[0;m\n";
		$nest .= "  ";
		next;
	    } elsif (index($_, "<== ") == 0) {
		$nest = substr($nest, 2);
		$foo = length ($nest);
		$foo = $foo / 2;
		printf ("%2d ", $foo);
		print $nest . %colours->{"<== "} . $_ . "\n"; # "\033[0;m\n";
		next;
	    }
	    print "$_\n";
	}

}

