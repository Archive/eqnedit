2000-12-21  Arturo Tena  <arturo@directmail.org>

	* Changes to allow compile under Red Hat Linux 7.0, with its
	cpp and gcc snapshot releases.
	* configure.in (COMPILE_DEBUG): Changed its place, in order to not
	interfere with autoconf macros, because the use of '-Werror' flag.
	* src/eqnedit-math-presentation-token.gob: Include config-full.h in
	sooner in the file, for include ctype.h.
	* src/eqnedit-opener-xml.gob (build_model): Use XMLCHAR_CHAR to
	avoid warnings.
	* src/eqnedit.c (print_canvasview_tree, print_canvas_tree): Added
	some casts to void* in order to avoid some warnings.

2000-09-21  Arturo Tena  <arturo@directmail.org>

	* po/README: New file.

2000-08-31  Arturo Tena  <arturo@directmail.org>

	* TODO: Updated.

	* Beginning of the cursor support. Many changes to list.
	* src/eqnedit-canvasview-cursor.gob: Ditto.
	* src/Makefile.am: Ditto.
	* src/eqnedit-canvasview-container.gob: Ditto.
	* src/eqnedit-canvasview-document.gob: Ditto.
	* src/eqnedit-canvasview-fraction.gob: Ditto.
	* src/eqnedit-canvasview-glyph.gob: Ditto.
	* src/eqnedit-canvasview-render.gob: Ditto.
	* src/eqnedit-canvasview-rendernull.gob: Ditto.
	* src/eqnedit-canvasview-rendersimple.gob: Ditto.
	* src/eqnedit-canvasview-token.gob: Ditto.
	* src/eqnedit-document-object.gob: Ditto.
	* src/eqnedit-iterator-object.gob: Ditto.
	* src/eqnedit-math-container.gob: Ditto.
	* src/eqnedit-math-document.gob: Ditto.
	* src/eqnedit-math-element.gob: Ditto.
	* src/eqnedit-math-fraction.gob: Ditto.

	* tests: New directory.
	* configure.in: Added --enable-tests flag.
	  (AC_OUTPUT): Added tests/Makefile.
	* Makefile.am (SUBDIRS): Added tests directory.
	* tests/Makefile.am: New file.
	* tests/only-canvas.c: New file.

2000-08-31  Arturo Tena  <arturo@directmail.org>

	* README.CVS: List the last stable tag.

2000-08-24  Arturo Tena  <arturo@directmail.org>

	* Moved CVS tag: before_cursor_implementation.

2000-08-24  Arturo Tena  <arturo@directmail.org>

	* src/eqnedit-document-object.gob
	  (construct): Create a second view to test multiple views.
	  (clean_as_new): Ditto.
	  (set_model): Ditto.
	  (doublev): New data member, second view container.
	* src/eqnedit.glade: Make a bigger main window, to show both views.

2000-08-24  Arturo Tena  <arturo@directmail.org>

	* src/eqnedit-canvasview-glyph.gob (pre_construct): Fix a memory leak
	  caused by GtkObject casts macros.

2000-08-23  Arturo Tena  <arturo@directmail.org>

	* src/eqnedit-canvasview-container.gob (event_handler_event_cb, update):
	  Set document dirty and hide box when adding a child, not when
	  handling the key.

	* src/eqnedit-canvasview-container.gob (handle_key): Detect End and
	  Insert keys.

	* src/eqnedit-canvasview-fraction.gob (construct): Do not set size of
	  the line, since it's set when rendering anyway.

2000-08-21  Arturo Tena  <arturo@directmail.org>

	* New CVS tag: before_cursor_implementation.

2000-08-21  Arturo Tena  <arturo@directmail.org>

	* TODO: Updated.

2000-08-21  Arturo Tena  <arturo@directmail.org>

	* NEWS: Updated.

2000-08-18  Arturo Tena  <arturo@directmail.org>

	* TODO: Updated.

	* configure.in: Check popt.h.
	* src/config-full.h: Conditionaly include popt.h.
	* src/eqnedit-glade.c (eqnedit_glade_init_with_popt): New call.
	* src/eqnedit-glade.h: Ditto.
	* Construct Eqnedit application.
	* src/eqnedit.c
	  (cwd): New call.
	  (EqneditConstructData): New struct.
	  (eqnedit_construct): New call.
	  (eqnedit_postconstruct): New call.
	  (main): Ditto.

	* Avoid to render the CanvasView tree when destroying it, because that
	  only make slower the destroy operation.
	* src/eqnedit-canvasview-container.gob
	  (shutdown): Disable render.
	  (construct): Disable render, but render if there's no parent.
	  (create_current_view): Enable render.
	  (add_child): Render if the render is enabled.
	  (remove_child): Render if the render is enabled.
	* src/eqnedit-canvasview-fraction.gob
	  (shutdown): Disable render.
	  (construct): Disable render.
	  (create_current_view): Enable render.
	  (add_child): Render if the render is enabled.
	  (remove_child): Render if the render is enabled.
	* src/eqnedit-canvasview-glyph.gob
	  (enable_render): New data member.

	* src/eqnedit-canvasview-glyph.gob
	  (shutdown): Unregister glyphs against the render when destroyed.
	  (post_construct): Render if the render is enabled.
	* src/eqnedit-canvasview-render.gob (unregister_glyph): New call.
	* src/eqnedit-canvasview-rendernull.gob (unregister_glyph): Ditto.
	* src/eqnedit-canvasview-rendersimple.gob (unregister_glyph): Ditto.

	* src/eqnedit-canvasview-render.gob: s/add/register_child
	* src/eqnedit-canvasview-rendernull.gob: Ditto.
	* src/eqnedit-canvasview-rendersimple.gob: Ditto.
	* src/eqnedit-canvasview-render.gob (register_glyph): Ditto.
	* src/eqnedit-canvasview-glyph.gob (construct): Ditto.

	* src/eqnedit-canvasview-rendersimple.gob (make_cache_dirty_func):
	  Check if the cache is actually found.

	* src/eqnedit-canvasview-rendersimple.gob (render):
	  Initialize root_glyph with NULL for safety.

	* src/eqnedit-iterator-object.gob
	  (find): New call.
	  (EqneditIteratorEachFunc): Ditto.
	  (EqneditIteratorCompFunc): Ditto.
	  (for_each): Ditto.

2000-08-15  Arturo Tena  <arturo@directmail.org>

	* src/Makefile.am: Fixed circular dependencies.

2000-08-11  Arturo Tena  <arturo@directmail.org>

	* src/eqnedit-canvasview-glyph.gob: Fixed a bug when destroying the
	  main window.
	  (group_destroy_cb): New call.
	  (destroy): Destroy the group only if it has not been destroyed.
	  (pre_construct): Connect the destroy event in the group with
	  group_destroy_cb.

2000-08-11  Arturo Tena  <arturo@directmail.org>

	* TODO: Updated.

	* Finished open support.
	* src/eqnedit-canvasview-container.gob
	  (update): Ditto.
	* src/eqnedit-canvasview-fraction.gob
	  (create_current_view): New call.
	* src/eqnedit-canvasview-glyph.gob
	  (post_construct): Call mvc_view_post_construct.
	  (build_canvasview): New call.
	* src/eqnedit-canvasview-token.gob
	  (create_current_view): New call.
	* src/eqnedit-canvasview-container.gob
	  (create_current_view): New call.
	  (create_new_canvasview): New call.
	* src/eqnedit-opener-xml.gob
	  (open): Ditto.
	  (get_top_node): Ditto.
	  (get_top_node): Changed signature: name from get_math_node.
	  (build_model): Ditto.
	* src/eqnedit-xmlview-object.gob
	  (construct): Call mvc_view_post_construct.

	* src/eqnedit-canvasview-container.gob (event_handler_event_cb): Hide
	  the box only when adding and not when deleting a child.

	* src/Makefile.am (eqnedit-math-container.c): Added dependencies.

	* src/eqnedit-document-object.gob
	  (private): s/type/filetype
	  (can_do_save): Ditto.
	  (save): Ditto.
	  (open): Ditto.
	  (set_filetype): Ditto.
	  (do_open): Changed signature: added a return parameter for the file
	  type.
	  (destroy): Fix a bug when trying to unref destroyed objects.

	* src/eqnedit-document-object.gob (open): Fixed bug: opening now
	  reset the filename.

	* src/eqnedit-opener-object.gob
	  (destroy): Destroy the new filetype data method.
	  (get_filetype): New call.
	* src/eqnedit-opener-object.gob
	  (construct): Changed signature: added filetype.
	* src/eqnedit-opener-xml.gob
	  (construct): Ditto.

	* src/xml-util.h
	  (XMLCHAR): Deleted macro.
	  (CHAR_XMLCHAR, XMLCHAR_CHAR): New macros.
	* src/eqnedit-saver-xml.gob
	  (save): Ditto.
	* src/eqnedit-xmlview-object.gob
	  (create_current_view): Ditto.

2000-08-09  Arturo Tena  <arturo@directmail.org>

	* src/eqnedit-canvasview-container.gob (shutdown, new,
	  new_without_parent, construct, add_child, remove_child, update):
	  Delete the foolish data member from EqneditCanvasViewGlyph class and
	  subclasses, which was used to enable or disable rendering when the
	  object was changed. They're now always rendered when changed, a
	  more appropiate approach is turn on and of the render itself.
	* src/eqnedit-canvasview-fraction.gob (shutdown, new, construct,
	  add_child, remove_child, update): Ditto.
	* src/eqnedit-canvasview-glyph.gob (construct, post_construct): Ditto.
	* src/eqnedit-canvasview-token.gob (new, construct): Ditto.
	* src/eqnedit-document-object.gob (construct, clean_as_new, set_model):
	  Ditto.
	* TODO: Updated.

	* src/eqnedit-math-presentationtoken.gob (get_content_type): Begining
	  the end of open support.
	* src/eqnedit-opener-xml.gob (build_model): Ditto.
	* src/eqnedit-xmlview-object.gob (construct): Ditto.
	  (create_curren_view): New call.

2000-08-09  Arturo Tena  <arturo@directmail.org>

	* TODO: Updated.

	* src/eqnedit-canvasview-fraction.gob (update): Fix a bug which prevent
	  numerator be rendered when required.

	* src/eqnedit-canvasview-container.gob (handle_key): Don't pass Tab
	  key, because it is printed.

2000-08-09  Arturo Tena  <arturo@directmail.org>

	* src/eqnedit-gui.c (eqnedit_gui_open): Ask the user if discards the
	  changes if the document is dirty.

2000-08-09  Arturo Tena  <arturo@directmail.org>

	* src/xml-util.h (XMLCHAR_CMP): Use casts to compile using
	  --enable-iso-c.

2000-08-09  Arturo Tena  <arturo@directmail.org>

	* New CVS tag: after_homogenization.

2000-08-09  Arturo Tena  <arturo@directmail.org>

	* TODO: Updated.

	* configure.in: Check ctype and standard headers.
	* src/config-full.h: Ditto.

	* doc/C/Makefile.am (LDFLAGS): Added needed objects.

	* src/Makefile.am: New classes: EqneditIteratorNull and
	  EqneditOpenerXML.
	* src/eqnedit-iterator-null.gob, src/eqnedit-opener-xml.gob: Ditto.

	* src/Makefile.am: New file: xml-util.h.

	* src/Makefile.am: Splitted gob_generated_files into .c and .h files.
	* src/Makefile.am: New rule: generated-c-files.

	* Check ref/unref correctness, grouped methods in interfaces (not all
	  classes), some (not all which should) get methods return const
	  pointers, homogenized code style, finished new support, finished
	  save support and almost finished open support. (Note: if we do
	  unref to the top math container, it first deattach the views and
	  destroy them, and then destroy the model):
		src/eqnedit-gui.c
		src/eqnedit-canvasview-container.gob
		src/eqnedit-canvasview-cursor.gob
		src/eqnedit-canvasview-fraction.gob
		src/eqnedit-canvasview-glyph.gob
		src/eqnedit-canvasview-render.gob
		src/eqnedit-canvasview-rendernull.gob
		src/eqnedit-canvasview-rendersimple.gob
		src/eqnedit-canvasview-token.gob
		src/eqnedit-document-object.gob
		src/eqnedit-iterator-glist.gob
		src/eqnedit-iterator-object.gob
		src/eqnedit-iterator-parentsglyph.gob
		src/eqnedit-math-container.gob
		src/eqnedit-math-element.gob
		src/eqnedit-math-fraction.gob
		src/eqnedit-math-presentation.gob
		src/eqnedit-math-presentationtoken.gob
		src/eqnedit-opener-object.gob
		src/eqnedit-saver-object.gob
		src/eqnedit-saver-xml.gob
		src/eqnedit-xmlview-object.gob
		src/gnome-canvas-acetate.gob
		src/eqnedit-opener-factory.h
		src/eqnedit-opener-factory.c
		src/eqnedit-saver-factory.c
		src/eqnedit-saver-factory.h
	* src/eqnedit.c: Ditto.
	* src/eqnedit-gui.c: Finished open/save/new support. Correctly unref
	  widgets.
	* src/xml-util.h: New file.
	* src/eqnedit.glade: Finished new support.

	* src/my-func.c (main): Fix a bug in the test program: correctly
	  unref widgets.

2000-08-09  Arturo Tena  <arturo@directmail.org>

	* New CVS tag: before_homogenization.

2000-08-04  Arturo Tena  <arturo@directmail.org>

	* NEWS: Updated, created 0.0.0.

	* src/eqnedit-opener-object.gob: Changed filename to gchar*.
	  (dispose): Ditto.
	  (construct): Created, callers changed.
	  (new): Ditto.

	* src/eqnedit-saver-xml.gob: Create the XML document and the root node
	  in EqneditSaverXML, instead in EqneditXMLViewObject.
	  (save): Ditto.
	* src/eqnedit-xmlview-object.gob (construct, get_xmldoc): Ditto.
	* src/eqnedit-saver-xml.gob: (eqn_ns): New data member.

2000-08-04  Arturo Tena  <arturo@directmail.org>

	* TODO: Updated.

	* src/eqnedit-canvasview-container.gob
	  (hide_box): New call.
	  (event_handler_event_cb): Now respond to GDK_KEY_PRESS, as GGAD say
	  not all harwdare generate GDK_KEY_RELEASE events. Return TRUE or
	  FALSE according if the key was handled, and call set_document_dirty if
	  the document was changed.
	  (XXX_DEBUG_MODIFIER, XXX_DEBUG_MODIFIERS, STATE_HAS_MODIFIER): New
	  macros.
	  (handle_key): Now detects all the appripate keys.
	  (handle_key): Fixed bug which printed the X names of the keys instead
	  its associated string.

	* src/eqnedit-xmlview-object.gob (construct): Changed the URI associated
	  to the eqn namespace.

	* src/eqnedit.glade: Fixed bug, it didn't check if the document was
	  dirty if exiting using "File/Quit" menu option.

	* MAINTAINERS: New file.
	* README: Updated.

2000-08-03  Arturo Tena  <arturo@directmail.org>

	* src/eqnedit-xmlview-object.gob (construct): Fixed math namespace.

2000-08-03  Arturo Tena  <arturo@directmail.org>

	* HACKING: Updated diff instructions.
	* TODO: Updated.

	* acinclude.m4 (COMPILE_WARNINGS): Added warnings, and make the
	  pedantic warnings errors.
	* autogen.sh: Not compile with --enable-iso-c by default.

	* configure.in: Added commented out check func.

	* doc/C/Makefile.am: Make compile scan program.
	* doc/C/eqnedit.types.autogenerate: Improved headers search.

	* src/config-full.h: Added #include: stdlib.h.

	* src/eqnedit-iterator-object.gob (dispose): Changed *_free calls for
	  *_dispose, to avoid clash names with the stdlib func free.
	* src/eqnedit-opener-object.gob (dispose): Ditto.
	* src/eqnedit-saver-object.gob (dispose): Ditto.
	* src/eqnedit-canvasview-rendersimple.gob (make_cache_dirty,
	  actual_render): Ditto.

	* eqnedit-math-presentationtoken.c (get_content): Changed name from
	  get_content_gstring, in order to return a plain gchar*, instead a
	  GString.
	* src/eqnedit-canvasview-token.gob (content): Ditto.
	* src/eqnedit.c: Ditto.

	* src/eqnedit-gui.c (eqnedit_gui_test_quit, try_open,
	  eqnedit_gui_open_check, eqnedit_gui_open, try_save,
	  eqnedit_gui_save_check, do_save_as, eqnedit_gui_save_as): Finished
	  support for open and save operations.
	* src/eqnedit-saver-xml.gob (save): Ditto.
	* src/Makefile.am (eqnedit_SOURCES): Added my-func.c, my-func.h,
	  deleted gtk-file-selection.c, eqnedit-opener-factory.h.
	* src/eqnedit-canvasview-container.gob
	  (set_document_dirty): New call.
	  (event_handler_event_cb): Call set_document_dirty if document changed.
	* src/eqnedit-document-object.gob: New file.
	* src/Makefile.am (gob_generated_files, gob_files,
	   eqnedit-canvasview-container.c): Added rules for
	   eqnedit-document-object-private class.
	* src/eqnedit-saver-xml.gob (construct): New call.
	* src/eqnedit-xmlview-object.gob: Now subclass from MVCView.
	  (construct, dispose, build_xml_math_doc, get_xmldoc, update): New
	  calls.

	* src/eqnedit-gui.c (about_dialog, manual_dialog,
	  eqnedit_dialog_not_directory, eqnedit_dialog_gui_yes_no,
	  eqnedit_gui_about_close, eqnedit_gui_about, eqnedit_gui_manual_close,
	  eqnedit_gui_manual): Simple dialogs moved from eqnedit.glade to be
	  created by code.
	* src/eqnedit.glade: Ditto.

	* src/eqnedit-gui.c (eqnedit_gui_show_widget): New commented out call.

	* src/eqnedit-math-fraction.gob
	  (numerator, denominator): made protected instead public.
	  (get_numerator, get_denominator): New calls.

	* src/eqnedit-math-presentationtoken.gob
	  (eqnedit_math_presentationtoken_content_type): New enum.
	  (content): Changed to char*.
	  (set_content, is_int_or_double, get_content_type): New calls,
	  analize the content and set the type.
	  (construct): Call set_content.

	* src/eqnedit-saver-factory.c, src/eqnedit-saver-factory.h
	  (eqnedit_saver_factory_new_saver): Changed signature: added
	  EqneditDocumentObject* parameter, changed "XML" type to ".eqnedit".


	* src/eqnedit-saver-object.gob (filename): Changed to char*.

	* src/eqnedit-saver-object.gob (construct): Changed signature:
	  filename is now const char*.

	* src/eqnedit.c
	  (global_root_math_container, global_root_canvasview_container,
	  create_math_model, create_canvas_view): Deleted.
	  (main.c): The main document is now attached to the main window,
	  and not as a global variable, which is cleanner. Create a document,
	  instead creating the math model and the view by hand.

	* src/eqnedit.c
	  (delete_event_cb): Disabled tree print, need to rethink this.

	* src/gnome-canvas-acetate.gob (update): Fix warning.

	* src/my-func.c, src/my-func.h: New files.
	* src/gtk-file-selection.c, src/gtk-file-selection.h: Deleted file,
	  functions are now in my-func.c and my-func.h.

2000-08-01  Arturo Tena  <arturo@directmail.org>

	* src/eqnedit-canvasview-container.gob
	  (construct): Not set size while construction, but it's asked while
	  rendering.
          (get_size_minimal): New call.
	* src/eqnedit-canvasview-rendersimple.gob (actual_render): Ask
	  container minimal size while rendering.

	* src/eqnedit-glade.c, src/eqnedit-glade.h
	  (eqnedit_glade_get_widget, eqnedit_glade_get_widget_singleton):
	  Signatures changed, added GladeXML **pxml parameter, to
	  save the GladeXML object in order to be able to get more widgets
	  from the already created GladeXML tree.
	* src/eqnedit-gui.c: Callers changed.
	* src/eqnedit.c (main): Callers changed. This fixes the bug in which
	  the new created canvas doesn't actually belongs to the previously
	  created main window.

2000-07-31  Arturo Tena  <arturo@directmail.org>

	* src/eqnedit-gui.c: New file.
	* src/eqnedit.glade: Changed signal handlers, manual is now a
	  GnomeMessageBox, created file_selector, msg_not_directory,
	  quest_file_exist widgets
	* src/Makefile.am: added object creation rules for:
		eqnedit-xmlview-object
		eqnedit-saver-object
		eqnedit-saver-xml
		eqnedit-opener-object
	* src/Makefile.am: added to eqnedit_SOURCES:
		eqnedit-saver-factory.h
		eqnedit-opener-factory.c
		eqnedit-opener-factory.h
		gtk-file-selection.c
		gtk-file-selection.h
		eqnedit-gui.c
	* New files:
		src/eqnedit-opener-factory.c
		src/eqnedit-opener-factory.h
		src/eqnedit-opener-object.gob
		src/eqnedit-saver-factory.c
		src/eqnedit-saver-factory.h
		src/eqnedit-saver-object.gob
		src/eqnedit-saver-xml.gob
		src/eqnedit-xmlview-object.gob

	* TODO: Updated.

	* configure.in: check string.h.
	* src/config-full.h: New file.

	* src/eqnedit-glade.c, src/eqnedit-glade.h:
	  (eqnedit_glade_init): Changed signature, don't take
	  nor process filename.
	  (eqnedit_glade_get_widget): New call, create the widget given a
	  filename.
	  (eqnedit_glade_get_widget_singleton): New call, create a single
	  instance of the widget.

	* src/gtk-file-selection.c, src/gtk-file-selection.h: New files,
	  needed a gtk_file_selection_run call.

	* src/eqnedit.c
	  (create_math_model, create_canvas_view): New calls, replace
	  create_canvas_items.
	  (main): Use the new eqnedit-glade.c stuff.

	* README.CVS: New file.

2000-07-29  Arturo Tena  <arturo@directmail.org>

	* HACKING: Updated.

	* autogen.sh: New file.
	* doc/C/eqnedit-docs.sgml, doc/C/eqnedit.types.autogenerate: New files.

2000-07-29  Arturo Tena  <arturo@directmail.org>

	* INSTALL: Removed innecesary file.
	* DEPENDS: New file.
	* README: Updated.

2000-07-29  Arturo Tena  <arturo@directmail.org>

	* New CVS tag: EQNEDIT_0_0_0.

	* New files:
		ChangeLog
		AUTHORS
		COPYING
		Makefile.am
		HACKING
		INSTALL
		acinclude.m4
		NEWS
		README
		TODO
		doc
		doc/C
		doc/C/Makefile.am
		doc/Makefile.am
		doc/MathML2-DOM.txt
		src
		src/mvc
		src/Makefile.am
		src/eqnedit-canvasview-container.gob
		src/eqnedit-canvasview-cursor.gob
		src/eqnedit-canvasview-fraction.gob
		src/eqnedit-canvasview-glyph.gob
		src/eqnedit-canvasview-render.gob
		src/eqnedit-canvasview-rendernull.gob
		src/eqnedit-canvasview-rendersimple.gob
		src/eqnedit-canvasview-token.gob
		src/eqnedit-glade.c
		src/eqnedit-glade.h
		src/eqnedit-iterator-glist.gob
		src/eqnedit-iterator-object.gob
		src/eqnedit-iterator-parentsglyph.gob
		src/eqnedit-math-container.gob
		src/eqnedit-math-element.gob
		src/eqnedit-math-fraction.gob
		src/eqnedit-math-presentation.gob
		src/eqnedit-math-presentationtoken.gob
		src/eqnedit.c
		src/eqnedit.glade
		src/gnome-canvas-acetate.gob
		src/util-trace.h
		configure.in
		tools
		tools/Makefile.am
		tools/util-trace-logmangle.pl
