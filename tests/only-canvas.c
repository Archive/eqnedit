#include <gnome.h>
#include "eqnedit-document-object.h"


#undef MAKEBUTTON
#if 0
#  define MAKEBUTTON
#endif


static gint
delete_event_cb (GtkWidget *window, GdkEventAny *e, gpointer data)
{
	gtk_main_quit ();

	return FALSE;
} /* delete_event_cb */


#ifdef MAKEBUTTON
static void
button_clicked_cb (GtkButton *button, gpointer user_data)
{
	g_message ("Button clicked");
} /* button_clicked_cb */
#endif


int
main (int argc, char *argv[])
{
	GtkWidget		*window;
	GtkWidget		*sw;
	GtkWidget		*vbox;
	GtkWidget		*canvas;
	EqneditDocumentObject	*document;

	gnome_init ("test-eqnedit", "0.0.0", argc, argv);

	gdk_rgb_init ();

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (window), "Eqnedit test");
	gtk_window_set_policy (GTK_WINDOW (window), TRUE, TRUE, TRUE);
	gtk_signal_connect (GTK_OBJECT (window), "delete_event",
			    GTK_SIGNAL_FUNC (delete_event_cb), NULL);
	gtk_window_set_default_size (GTK_WINDOW (window), 300, 300);

	vbox = gtk_vbox_new (FALSE, FALSE);
	gtk_container_add (GTK_CONTAINER (window), vbox);

#ifdef MAKEBUTTON
{
	GtkWidget	*button;

	button = gtk_button_new_with_label ("Test");
	gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (button_clicked_cb), NULL);
}
#endif

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (vbox), sw);

	gtk_widget_push_visual (gdk_rgb_get_visual ());
	gtk_widget_push_colormap (gdk_rgb_get_cmap ());
	canvas = gnome_canvas_new_aa ();
	gtk_widget_pop_colormap ();
	gtk_widget_pop_visual ();
	gnome_canvas_set_scroll_region (GNOME_CANVAS (canvas),
					0.0, 0.0, 600.0, 450.0);
	gtk_container_add (GTK_CONTAINER (sw), canvas);

	document = eqnedit_document_object_new (GNOME_CANVAS (canvas),
						GTK_WINDOW (window));
	gtk_widget_show_all (window);

	gtk_main ();

	gtk_object_unref (GTK_OBJECT (document));

	return 0;
} /* main */

