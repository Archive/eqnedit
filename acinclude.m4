dnl *********************************************************************
dnl COMPILE_WARNINGS
dnl Turn on many useful compiler warnings
dnl For now, only works on GCC

AC_DEFUN([COMPILE_WARNINGS],[

  enableval=no
  AC_ARG_ENABLE(compile-warnings, 
    [  --enable-compile-warnings     Turn on compiler warnings])
  AC_MSG_CHECKING(what warning flags to pass to the C compiler)
  warnCFLAGS=
  if test "x$enableval" = "xyes"; then
    if test "x$GCC" = "xyes"; then
      warnCFLAGS="-Wall -Wunused -Wmissing-prototypes -Wmissing-declarations -Wpointer-arith -Wsign-compare -Wbad-function-cast -Wconversion"
    fi
  fi
  AC_MSG_RESULT($warnCFLAGS)

  enableval=no
  AC_ARG_ENABLE(iso-c,
    [  --enable-iso-c          Try to warn if code is not ISO C ])
  AC_MSG_CHECKING(what language compliance flags to pass to the C compiler)
  complCFLAGS=
  if test "x$enableval" = "xyes"; then
    if test "x$GCC" = "xyes"; then
        complCFLAGS="-ansi -pedantic-errors"
    fi
  fi
  AC_MSG_RESULT($complCFLAGS)


  if test "x$compile_warnings_set" != "xyes"; then
    CFLAGS="$CFLAGS $warnCFLAGS $complCFLAGS"
    compile_warnings_set=yes
    AC_SUBST(compile_warnings_set)
  fi

])


dnl *********************************************************************
dnl *********************************************************************
dnl COMPILE_DEBUG
dnl Turn on many userful debug compiler flags
dnl For now, only works on GCC

AC_DEFUN([COMPILE_DEBUG],[

  enableval=no
  AC_ARG_ENABLE(debug,
    [  --enable-debug          Enable compiler debug flags])
  AC_MSG_CHECKING(what debug flags to pass to the C compiler)
  debugCFLAGS=
  if test "x$enableval" = "xyes"; then
    if test "x$GCC" = "xyes"; then
      debugCFLAGS="$debugCFLAGS -g -O0 -Werror"
    fi
  fi
  AC_MSG_RESULT($debugCFLAGS)


  if test "x$compile_debug_set" != "xyes"; then
    CFLAGS="$CFLAGS $debugCFLAGS"
    compile_debug_set=yes
    AC_SUBST(compile_debug_set)
  fi

])


dnl *********************************************************************
dnl *********************************************************************
dnl UTIL_TRACE
dnl Enable util trace

AC_DEFUN([UTIL_TRACE],[

  enableval=no
  AC_ARG_ENABLE(util-trace,
    [  --enable-util-trace     Enable util trace])
  AC_MSG_CHECKING(what trace flags to pass to the C compiler)
  traceCFLAGS=
  if test "x$enableval" = "xyes"; then
    traceCFLAGS="$traceCFLAGS -DUTIL_TRACE"
  fi
  AC_MSG_RESULT($traceCFLAGS)


  if test "x$util_trace_set" != "xyes"; then
    CFLAGS="$CFLAGS $traceCFLAGS"
    util_trace_set=yes
    AC_SUBST(util_trace_set)
  fi

])


dnl *********************************************************************
