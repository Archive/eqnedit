dnl *********************************************************************
dnl Initialization

AC_INIT(src/eqnedit.c)
AM_INIT_AUTOMAKE(eqnedit, 0.0.0)
Package="Eqnedit"
AM_MAINTAINER_MODE
AM_CONFIG_HEADER(config.h)


dnl *********************************************************************
dnl Checks for programs.

AC_PROG_CC
AC_PROG_RANLIB


dnl *********************************************************************
dnl Set C compiler flags.

COMPILE_WARNINGS
UTIL_TRACE


dnl *********************************************************************
dnl Documentation.

# source dir
AC_ARG_WITH(eqnedit-dir, [  --with-eqnedit-dir=PATH path to eqnedit source directory ])
if test "x$with_eqnedit_dir" = "x" ; then
  EQNEDIT_DIR=`(cd $srcdir; pwd)`
else
  EQNEDIT_DIR=$with_eqnedit_dir
fi
AC_SUBST(EQNEDIT_DIR)

# gtk-doc check
dnl dnl AC_CHECK_PROG(GTKDOC, gtkdoc-mkdb, true, false)
dnl dnl AM_CONDITIONAL(HAVE_GTK_DOC, $GTKDOC)
dnl dnl AC_SUBST(HAVE_GTK_DOC)
dnl dnl ## Let people disable the gtk-doc stuff.
dnl dnl AC_ARG_ENABLE(gtk-doc, [  --enable-gtk-doc        Use gtk-doc to build documentation [default=auto]], enable_gtk_doc="$enableval", enable_gtk_doc=auto)
dnl dnl 
dnl dnl if test x$enable_gtk_doc = xauto ; then
dnl dnl   if test x$GTKDOC = xtrue ; then
dnl dnl     enable_gtk_doc=yes
dnl dnl   else
dnl dnl     enable_gtk_doc=no
dnl dnl   fi
dnl dnl fi
dnl dnl # NOTE: We need to use a separate automake conditional for this
dnl dnl #       to make this work with the tarballs.
dnl dnl AM_CONDITIONAL(ENABLE_GTK_DOC, test x$enable_gtk_doc = xyes)


dnl *********************************************************************
dnl Checks for tests.

AC_ARG_ENABLE(tests,
	[  --enable-tests      build tests [default=auto]],
	[case "${enableval}" in
		yes) tests=true ;;
		no)  tests=false ;;
		*) AC_MSG_ERROR(bad value ${enableval} for --enable-tests) ;;
		esac],[tests=false])
AM_CONDITIONAL(BUILD_TESTS, test x$tests = xtrue)


dnl *********************************************************************
dnl Instalation paths.

dnl Documentation path
# html dir
AC_ARG_WITH(html-dir, [  --with-html-dir=PATH    path to installed docs ])
if test "x$with_html_dir" = "x" ; then
  HTML_DIR='${datadir}/gnome/help'
else
  HTML_DIR=$with_html_dir
fi
AC_SUBST(HTML_DIR)

dnl Glade path
eqnedit_gladedir="${datadir}/eqnedit/glade/"
AC_SUBST(eqnedit_gladedir)


dnl *********************************************************************
dnl Checks for libraries.

dnl LibGlade checking
dnl (from gnumeric/configure.in)
AC_MSG_CHECKING(for libglade >= 0.13)
if gnome-config --libs libglade > /dev/null 2>&1; then 
    vers=`gnome-config --modversion libglade | awk 'BEGIN { FS = "."; } { print $1 * 1000 + $2;}'`
    if test "$vers" -ge 13; then
        AC_MSG_RESULT(found)
    else
        AC_MSG_RESULT(not found)
        AC_MSG_ERROR(You need at least libglade 0.13 for this version of ${Package})
    fi
else
    AC_MSG_ERROR(Did not find libGlade installed)
fi

# Innecesary, libglade needs libxml
# dnl gnome-xml checking
# dnl (from gnumeric/configure.in)
# AC_MSG_CHECKING(for libxml (aka gnome-xml) libraries 1.8.7 <= version < 2.0.0)
# if xml-config --libs print > /dev/null 2>&1; then 
#     vers=`xml-config --version | sed -e "s/libxml //" | awk 'BEGIN { FS = "."; } { printf "%d", ($1 * 1000 + $2) * 1000 + $3;}'`
#     if test "$vers" -ge 1008007; then
# 	if test "$vers" -lt 2000000; then
# 	    AC_MSG_RESULT(found)
# 	else
# 	    AC_MSG_ERROR(This version of ${Package} does not support libxml (aka gnome-xml) 2.x.x yet.)
# 	fi
#     else
#         AC_MSG_ERROR(You need at least libxml (aka gnome-xml) 1.8.7 for this version of ${Package}.)
#     fi
# else
#     AC_MSG_ERROR(Did not find libxml (aka gnome-xml) installed)
# fi


dnl *********************************************************************
dnl Checks for header files.

AC_HEADER_STDC
AC_CHECK_HEADERS(ctype.h)
AC_CHECK_HEADERS(string.h)
AC_CHECK_HEADERS(popt.h)


dnl *********************************************************************
dnl Checks for typedefs, structures, and compiler characteristics.


dnl *********************************************************************
dnl Checks for library functions.

# AC_REPLACE_FUNCS(strcmp)
# AC_REPLACE_FUNCS(free)


dnl *********************************************************************
dnl Set extra C compiler flags.

COMPILE_DEBUG


dnl *********************************************************************
dnl Set variables.

EXTRA_GNOME_CFLAGS=`gnome-config --cflags gnomeui libglade`
EXTRA_GNOME_LIBS=`gnome-config --libs gnomeui libglade`
# Innecesary, libglade needs libxml
# EXTRA_GNOME_CFLAGS=`gnome-config --cflags gnomeui libglade xml`
# EXTRA_GNOME_LIBS=`gnome-config --libs gnomeui libglade xml`
AC_SUBST(EXTRA_GNOME_CFLAGS)
AC_SUBST(EXTRA_GNOME_LIBS)

MVC_CFLAGS="-I\${top_srcdir}/src/mvc/src"
MVC_LIB="\${top_builddir}/src/mvc/src/libmvc.a"
AC_SUBST(MVC_CFLAGS)
AC_SUBST(MVC_LIB)


dnl *********************************************************************
dnl Configure subpackages
AC_CONFIG_SUBDIRS(src/mvc)


dnl *********************************************************************
dnl Generate files

AC_OUTPUT(
src/Makefile
doc/Makefile
doc/C/Makefile
tools/Makefile
tests/Makefile
Makefile
)
